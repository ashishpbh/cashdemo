/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import AppNavigation from "./src/DrawerComponent"

const App=() => {
  return (
      <SafeAreaView style={{flex:1}}>
        <AppNavigation/>
      </SafeAreaView>
  );
};

export default  App;
