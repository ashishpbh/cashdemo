import React from 'react';
import {View, Text} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {HomeScreen, AccountScreen} from './screen/index';
import AddButton from './AddButton';
import {Routes} from './Routes';
let bottomTabBar = {
  [Routes.home]: {
    screen: HomeScreen,
    navigationOptions: () => ({
      tabBarIcon: (
        <View>
          <Text>image1</Text>
        </View>
      ),
    }),
  },
  add: {
    screen: () => null,
    navigationOptions: () => ({
      tabBarIcon: <AddButton />,
    }),
    params: {
      navigationDisabled: true,
    },
  },
  [Routes.account]: {
    screen: AccountScreen,
    navigationOptions: () => ({
      tabBarIcon: (
        <View>
          <Text>img2</Text>
        </View>
      ),
    }),
  },
};

let BottomTabBarNavigation = createBottomTabNavigator(bottomTabBar, {
  tabBarOptions: {
    activeTintColor: '#FF6F00',
    inactiveTintColor: '#263238',
  },
});
export default BottomTabBarNavigation;
