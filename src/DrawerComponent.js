import React from "react";
import {View,Text,TouchableOpacity } from  "react-native";
import {createDrawerNavigator} from "react-navigation-drawer"
import {createAppContainer} from "react-navigation";
import BottomTabBarNavigation from "./BottomTabBar" 

class DrawerScreen extends React.Component{
    newToggleDra = () => {
      this.props.navigation &&this.props.navigation.navigate('Account');
      this.props.navigation&& this.props.navigation.closeDrawer();
    };

    render() {
      return (
        <View style={{flex:1 }}>
          <TouchableOpacity onPress={this.newToggleDra}>
            <View style={{alignItems:"center",justifyContent:"center"}}><Text>Account screen</Text></View> 
          </TouchableOpacity>
        </View>
      );
    }
  }



const MainNavigator = createDrawerNavigator({
   Home: { screen: BottomTabBarNavigation},
  },
    {
    contentComponent: DrawerScreen,
    drawerWidth: 250
    }
   );

let AppNavigation = createAppContainer(MainNavigator);

export default AppNavigation;