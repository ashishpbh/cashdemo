import React from 'react';
import {Animated, TouchableHighlight, Text, View} from 'react-native';

class AddButton extends React.Component {
  mode = new Animated.Value(0);
  toggleView = () => {
    Animated.timing(this.mode, {
      toValue: this.mode._value === 0 ? 1 : 0,
      duration: 300,
    }).start();
  };
  render() {
    const firstX = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [20, -40],
    });
    const firstY = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -30],
    });
    const secondX = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [20, 20],
    });
    const secondY = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -55],
    });
    const thirdX = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [20, 80],
    });
    const thirdY = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -30],
    });
    const opacity = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    let SIZE = 70;
    let rotation = this.mode.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '45deg'],
    });
    let height = this.mode.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 40],
      });
      let width = this.mode.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 40],
      });
    return (
      <View
        style={{
          position: 'absolute',
          alignItems: 'center',
        }}>
        <Animated.View
          style={{
            position: 'absolute',
            left: firstX,
            top: firstY,
            opacity,
          }}>
          <TouchableHighlight
            onPress={() => {}}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              width: SIZE / 2,
              height: SIZE / 2,
              borderRadius: SIZE / 4,
              backgroundColor: '#48A2F8',
            }}>
            <Text>first</Text>
          </TouchableHighlight>
        </Animated.View>
        <Animated.View
          style={{
            position: 'absolute',
            left: secondX,
            top: secondY,
            opacity,
          }}>
          <TouchableHighlight
            onPress={() => {}}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              width: SIZE / 2,
              height: SIZE / 2,
              borderRadius: SIZE / 4,
              backgroundColor: '#48A2F8',
            }}>
            <Text>sec</Text>
          </TouchableHighlight>
        </Animated.View>
        <Animated.View
          style={{
            position: 'absolute',
            left: thirdX,
            top: thirdY,
            opacity,
          }}>
          <TouchableHighlight
            onPress={() => {}}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              width: SIZE / 2,
              height: SIZE / 2,
              borderRadius: SIZE / 4,
              backgroundColor: '#48A2F8',
            }}>
            <Text>third</Text>
          </TouchableHighlight>
        </Animated.View>
        <TouchableHighlight
          onPress={this.toggleView}
          underlayColor="#2882D8"
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: SIZE,
            height: SIZE,
            borderRadius: SIZE / 2,
            backgroundColor: '#48A2F8',
          }}>
          <Animated.View
            style={{
              transform: [{rotate: rotation}],
            }}>
            <Text style={{color:"white"}}>+</Text>
          </Animated.View>
        </TouchableHighlight>
      </View>
    );
  }
}
export default AddButton;
