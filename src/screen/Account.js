import React from 'react';
import {
  View,
  Text,
  Button,
  Dimensions,
  ScrollView,
  Animated,
} from 'react-native';

export default () => {
  class Account extends React.Component {
    state = {
      scrollY: new Animated.Value(0),
    };

    cardComponent = i => {
      return (
        <View
          key={i}
          style={{
            margin: 10,
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: '#000',
            borderWidth: 0,
            borderRadius: 5,
            borderColor: 'transparent',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 2,
            elevation: 5,
          }}>
          <Text> dummy text</Text>
        </View>
      );
    };
    render() {
      let HEADER_EXPANDED_HEIGHT = 200;
      let HEADER_COLLAPSED_HEIGHT = 50;
      const {width: SCREEN_WIDTH} = Dimensions.get('screen');
      const headerHeight = this.state.scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
        extrapolate: 'clamp',
      });
      const headerTitleOpacity = this.state.scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [0, 1],
        extrapolate: 'clamp',
      });
      const heroTitleOpacity = this.state.scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [1, 0],
        extrapolate: 'clamp',
      });
      let headerTitle = 'Header';

      let cardComponent = [];
      for (let i = 0; i < 30; i++) {
        cardComponent.push(this.cardComponent(i));
      }
      return (
        <View style={{flex: 1}}>
          <Animated.View
            style={{
              height: headerHeight,
              backgroundColor: 'grey',
             // position: 'absolute',
              top: 0,
              left: 0,
            }}>
            <Animated.Text
              style={{
                textAlign: 'center',
                marginTop: 28,
                opacity: headerTitleOpacity,
              }}>
              {headerTitle}
            </Animated.Text>
            <Animated.Text
              style={{
                position: 'absolute',
                bottom: 16,
                left: 16,
                opacity: heroTitleOpacity,
              }}>
              {headerTitle}
            </Animated.Text>
          </Animated.View>
          <ScrollView
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: {
                     y: this.state.scrollY,
                  },
                },
              },
            ])}>
            {cardComponent}
          </ScrollView>
        </View>
      );
    }
  }
  return Account;
};
