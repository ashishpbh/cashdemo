
import React from 'react';
import {View, Text,Image} from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {createAppContainer} from "react-navigation"
import Shimmer from "./Shimmer";
export default({text,HeaderComponent})=>{
    class HomeScreen extends React.Component {
      // static navigationOptions = {
      //   drawerLabel: 'Home',
      //   drawerIcon: ({ tintColor }) => (
      //    <View><Text>new</Text></View>
      //   ),
      // };
        render() {
          return (
            <View style={{flex:1}}>
               {HeaderComponent?<HeaderComponent {...this.props}/>:void 0}
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center',flexDirection:"row"}}>
              <Shimmer autoRun={true}  style={{height: 40,width:40}} visible={false}>
                 <View style={{borderWidth:2, borderColor:"red",height: 40,width:40}}/>
                 </Shimmer>
              <Text>
                {text}
               </Text>
              
            </View>
            </View>
            
          );
        }
      }
      return HomeScreen;

     
  
}
