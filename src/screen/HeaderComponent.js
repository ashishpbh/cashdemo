import React from 'react';
import {View, Text,Image,TouchableHighlight,TouchableWithoutFeedback} from 'react-native';
export default () => {
  class HeaderComponent extends React.Component {
    openDrawer=()=>{
        console.warn("we are called");
       console.warn(" this.props.navigationProps", this.props.navigationProps); 
       this.props.navigation && this.props.navigation.openDrawer();
    }
    render() {
      let {title="Home"}=this.props;
      return (
        <View style={{backgroundColor:"green", flexDirection: 'row',padding:10,justifyContent:"space-between"}}>
         <TouchableWithoutFeedback onPress={this.openDrawer} style={{flex:1}}><Text >menu</Text></TouchableWithoutFeedback>
         <View style={{ flex:1,alignItems:"center"}}><Text>{title}</Text></View>
        </View>
      );
    }
  }
 return  HeaderComponent;
};
